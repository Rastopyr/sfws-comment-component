
### Template of comment

Template of comment from main repo

```
<div class="DataBox DataBox-Comments">
    <h2 class="CommentHeading" style="text-transform: none;">Комментарии</h2>
    <ul class="MessageList DataList Comments">
        {% if comments %}
        {% for comment in comments %}
        <li class="Item Alt ItemComment" id="Comment_8">
            <div class="Comment">
                <div class="Options"></div>
                <div class="Item-Header CommentHeader">
                    <div class="AuthorWrap">
                        <span class="Author">
                            <a title="bsmith" href="/profile/{{comment.user_id}}" class="PhotoWrap">
                                <img src="{{comment.userpic}}" alt="bsmith" class="ProfilePhoto ProfilePhotoMedium"/>
                            </a>
                            <a href="/profile/{{comment.user_id}}" class="Username">{{comment.user_id}}</a>
                        </span>
                        <span class="AuthorInfo"></span>
                    </div>
                    <div class="Meta CommentMeta CommentInfo">
                    <span class="MItem DateCreated">
                        <time title="" datetime="">
                            {{comment.comment_date|date:"yyyy-MM-dd HH:mm"}}
                        </time>
                    </span>
                    </div>
                </div>
                <div class="Item-BodyWrap">
                    <div class="Item-Body">
                        <div class="Message">
                            {{comment.comment}}
                        </div>
                        <div class="Reactions"></div>
                    </div>
                </div>
            </div>
        </li>
        {% endfor %}
        {% endif %}
        {% if user %}
        <li class="Item Alt ItemComment" id="Comment_1">
            <div class="Comment">
                <div class="Options"></div>
                <div class="Item-Header CommentHeader">
                    <div class="AuthorWrap">
                        <span class="Author">
                            <a title="bsmith" href="/profile/{{user.username}}" class="PhotoWrap">
                                <img src="{{user.userpic}}"
                                     alt="bsmith"
                                     class="ProfilePhoto ProfilePhotoMedium"/>
                            </a>
                            <a href="/profile/{{user.username}}" class="Username">{{user.username}}</a>
                        </span>
                        <span class="AuthorInfo"></span>
                    </div>
                    <!--<div class="Meta CommentMeta CommentInfo">-->
                    <!--<span class="MItem DateCreated">-->
               <!--<a href="/nexus/discussion/comment/8/#Comment_8" class="Permalink" name="Item_3" rel="nofollow">-->
                   <!--<time title="July  4, 2014  6:20PM" datetime="2014-07-04T15:20:32+00:00">July 2014</time>-->
               <!--</a>            </span>-->
                    <!--</div>-->
                </div>
                <div class="Item-BodyWrap" style="margin-top: 10px;">
                    <div class="Item-Body">
                        <form method="post" action="/idea/{{idea.id}}/comment">
                        {% if errors %}
                        <div class="Messages Errors">
                            <ul>
                                {% for error in errors %}
                                <li>{{error}}</li>
                                {% endfor %}
                            </ul>
                        </div>
                        {% endif %}
                        <div>
                            <ul>
                                  <li class="User-Email">
                                    <!--<label for="1Form_Email">Описание</label>-->
                                    <textarea id="1Form_Email"
                                              name="comment"
                                              placeholder="Ваш комментарий"
                                              class="InputBox123"
                                              style="width: 100% !important; height: 100px;">{{content}}</textarea>
                                </li>
                                <li>
                                    <input type="hidden" name="token" value="{{comment-form-token}}"/>
                                </li>
                                <li>{% safe %}{{csrf-field}}{% endsafe %}</li>
                                <li class="Buttons" style="margin-top: 10px;">
                                    <input type="submit" id="Form_SignUp" name="Sign_Up" value="Оставить комментарий"
                                           class="Button Primary" style="text-transform: none;"/>
                                </li>
                            </ul>
                        </div>
                    </form>
                        <!--<div class="Message">-->

                            <!--<textarea style="width: 100% !important"></textarea>-->
                            <!--<button href="/login" class="Button Primary SignInPopup" rel="nofollow">Оставить комментарий</button>-->
                        <!--</div>-->
                        <div class="Reactions"></div>
                    </div>
                </div>
            </div>
        </li>
        {% else %}
        <div class="Foot Closed">
            <div class="Note Closed SignInOrRegister">
                Необходимо <a href="/login" class="Popup">войти</a> или <a href="/signup">создать аккаунт</a>, чтобы оставить комментарий.
            </div>
        </div>
        {% endif %}
    </ul>
</div>
```
